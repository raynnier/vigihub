import os, threading
from time import sleep

"""
Electronics class will handle all electronics interface of vigihub
No business implementation here

==>>by R4yG<==
"""
class Electronics():
	def __init__(self, PROD=False):

		self.PROD    = PROD
		self.VERBOSE = True
		
		self.verbose("Loading Electronics Constructor Object")

		#setting board electronics
		self.GPIO_MODE_PATH     = os.path.normpath('/sys/devices/virtual/misc/gpio/mode')
		self.GPIO_PIN_PATH      = os.path.normpath('/sys/devices/virtual/misc/gpio/pin')
		self.PWM_PATH           = os.path.normpath('/sys/class/misc/pwmtimer/enable/')
		self.GPIO_FILENAME      = "gpio"
		self.GENERAL_SLEEP_TIME = 0.1
		self.BLINK_TIME         = 0.2

		#setting equivalences
		self.HIGH     = '1'
		self.LOW      = '0'
		self.INPUT    = '0'
		self.OUTPUT   = '1'
		self.INPUT_PU = '8'

		self.pinMode = []
		self.pinData = []

		#filling resources
		for i in range(0, 18):
			self.pinMode.append(os.path.join(self.GPIO_MODE_PATH,"{FILE_NAME}{NUMBER}".format(FILE_NAME=self.GPIO_FILENAME, NUMBER=i)))
			self.pinData.append(os.path.join(self.GPIO_PIN_PATH,"{FILE_NAME}{NUMBER}".format(FILE_NAME=self.GPIO_FILENAME, NUMBER=i)))

		### SENSORS SETTINGS ###
		self.MOTION_SENSOR = 2 #GPIO02, Motion Sensor
		self.GREEN_LED     = 3 #GPIO03, Green Led
		self.WHITE_LED     = 4 #GPIO04, White Led
		self.RED_LED       = 5 #GPIO05, Red Led
		self.BLUE_LED      = 6 #GPIO06, Blue Led
		self.LAMP_LED    = 7 #GPIO07, High Light

		### led status: ON, OFF, BLINK
		self.LED_STATUS = {
			"WHITE": "OFF",
			"RED":   "OFF",
			"BLUE":  "OFF",
			"GREEN": "OFF",
			"LAMP":  "OFF"
		}

		#setting Motion Sensor as input
		self.set_mode(self.INPUT, self.MOTION_SENSOR)

		#setting light as output
		self.set_mode(self.OUTPUT, self.WHITE_LED)
		self.set_mode(self.OUTPUT, self.RED_LED)
		self.set_mode(self.OUTPUT, self.BLUE_LED)
		self.set_mode(self.OUTPUT, self.GREEN_LED)
		self.set_mode(self.OUTPUT, self.LAMP_LED)

		# setting production parameters
		if self.PROD:
			self.VERBOSE = False
			self.ledstat = threading.Thread(target=self.process_led_status_prod)
			self.ledstat.start()
		else:
			self.ledstat = threading.Thread(target=self.process_led_status_development)
			self.ledstat.start()


	"""
	verbose method for development purposes
	"""
	def verbose(self, text):
		if self.VERBOSE:
			print("{VERBOSE}".format(VERBOSE=text))


	def set_led(self, led, status):
		try:
			status = str(status).upper()
			led    = str(led).upper()
			self.LED_STATUS[led] # validating the key

			if status not in ["ON", "OFF", "BLINK"]:
				self.verbose("Only On, Off or Blink allowed.")
			else:
				self.LED_STATUS[led] = status

		except:
			self.verbose("There is no led {LED}".format(LED=led))

	def process_led_status_development(self):
		for i in range(0,150):
			for led in self.LED_STATUS:
				led_stat = self.LED_STATUS.get(led)
				if led_stat.upper() == "BLINK":
					function_to_perform = str("self.get_{LED}_LED".format(LED=led)).lower()
					led_stat = "OFF" if eval(function_to_perform)() else "ON"
				function_to_perform = str("self.set_{LED}_LED".format(LED=led)).lower()
				eval(function_to_perform)(led_stat)
			sleep(self.BLINK_TIME)

	def process_led_status_prod(self):
		while True:
			for led in self.LED_STATUS:
				led_stat = self.LED_STATUS.get(led)
				if led_stat.upper() == "BLINK":
					function_to_perform = str("self.get_{LED}_LED".format(LED=led)).lower()
					led_stat = "OFF" if eval(function_to_perform)() else "ON"
				function_to_perform = str("self.set_{LED}_LED".format(LED=led)).lower()
				eval(function_to_perform)(led_stat)
			sleep(self.BLINK_TIME)


	"""
	set_mode method is used to adjust the pins as I/O
	"""
	def set_mode(self, mode, pin):
		pwm_file = "pwm{PIN}".format(PIN=pin)
		pwm_file_path = os.path.join(self.PWM_PATH, pwm_file)
		if os.path.isfile(pwm_file_path):
			with open(pwm_file_path, 'w+') as f:
				f.write('0')

		set_mode = "INPUT" if mode == self.INPUT else "OUTPUT"
		self.verbose("Setting GPIO{PIN} as {MODE}".format(PIN=pin, MODE=set_mode))
		with open(self.pinMode[pin], 'w+') as f:
			f.write('0' if mode == self.INPUT else '1')

	"""
	get_motion method is used to check if there is any presence around detected by the IR Motion Sensor
	returning True if there is any presence or False if there is all quiet around
	"""
	def get_motion(self):
		with open(self.pinData[self.MOTION_SENSOR], 'r') as f:
			return f.read(1) == '1'

	"""
	set_LAMP_LED method is used to turn on the big bulb to get light in order to take a clear picture in darkness
	the parameter is mode = "OFF" or mode = "ON"
	"""
	def set_lamp_led(self, mode="OFF"):
		pin = self.LAMP_LED
		with open(self.pinData[pin], 'w+') as f:
			f.write('0' if mode.upper() == "OFF" else '1')

	def get_lamp_led(self):
		pin = self.LAMP_LED
		with open(self.pinData[pin], 'r') as f:
			return f.read(1) == '1'

	def set_white_led(self, mode="OFF"):
		pin = self.WHITE_LED
		with open(self.pinData[pin], 'w+') as f:
			f.write('0' if mode.upper() == "OFF" else '1')

	def get_white_led(self):
		pin = self.WHITE_LED
		with open(self.pinData[pin], 'r') as f:
			return f.read(1) == '1'

	def set_blue_led(self, mode="OFF"):
		pin = self.BLUE_LED
		with open(self.pinData[pin], 'w+') as f:
			f.write('0' if mode.upper() == "OFF" else '1')

	def get_blue_led(self):
		pin = self.BLUE_LED
		with open(self.pinData[pin], 'r') as f:
			return f.read(1) == '1'

	def set_red_led(self, mode="OFF"):
		pin = self.RED_LED
		with open(self.pinData[pin], 'w+') as f:
			f.write('0' if mode.upper() == "OFF" else '1')

	def get_red_led(self):
		pin = self.RED_LED
		with open(self.pinData[pin], 'r') as f:
			return f.read(1) == '1'

	def set_green_led(self, mode="OFF"):
		pin = self.GREEN_LED
		with open(self.pinData[pin], 'w+') as f:
			f.write('0' if mode.upper() == "OFF" else '1')

	def get_green_led(self):
		pin = self.GREEN_LED
		with open(self.pinData[pin], 'r') as f:
			return f.read(1) == '1'
