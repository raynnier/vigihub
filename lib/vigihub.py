import requests, json, os
"""
ServerTrades class will handle all connections to the server
Business will be implemented on server side

==>>by R4yG<==
"""
class ServerTrades():

	def __init__(self):
		self.SERVER_UPLOAD_URL = "http://vigihub.com/get_from_cam/"
		self.SERVER_AWARE_URL  = "http://vigihub.com/set_online_aware/"

	def post_image_to_server(self, image, cam_id, token):
		files = {
			'photo': image,
			'Content-Type': 'image/jpeg'
		}
		r=requests.post(
			self.SERVER_UPLOAD_URL,
			verify=False, 
			files=files,
			data={
				"cam_id" : cam_id,
				"token" : token
			}
		)
		try:
			return json.loads(r.text)
		except:
			return None

	def set_online_aware(self, cam_id, token):
		r=requests.post(
			self.SERVER_AWARE_URL,
			verify=False, 
			data={
				"cam_id" : cam_id,
				"token" : token,
			}
		)
		try:
			return json.loads(r.text)
		except:
			return None

"""
VAction class is a miscelaneous class to hadle different actions
Business will be implemented on server side

==>>by R4yG<==
"""
class VAction():
	def __init__(self):
		self.TOKEN_ID_PATH = os.path.dirname(os.getcwd())
		self.TOKEN_FILE    = os.path.join(self.TOKEN_ID_PATH, "cam_token.tk")
		self.CAM_ID_FILE   = os.path.join(self.TOKEN_ID_PATH, "cam_id.tk")

	def store_token(self, token):
		with open(self.TOKEN_FILE, "w") as f:
			f.write(token)

	def get_cam_id(self):
		with open(self.CAM_ID_FILE, "r") as f:
			cam_id = f.readlines()[0].replace("\n", "")
		return cam_id

	def get_token(self):
		with open(self.TOKEN_FILE, "r") as f:
			token = f.readlines()[0].replace("\n", "")
		return token
