#!/usr/bin/python

import time, cv2, threading, os

from lib.electronics import Electronics
from lib.vigihub import ServerTrades,VAction
from time import sleep

AWARE_TIME_CICLE  = 60 #time in seconds to advice connection

sending_image     = False
aware_online_time = time.time() - AWARE_TIME_CICLE


def send_cam_online_aware(electronics_instance):
	global aware_online_time
	if time.time() - aware_online_time >= AWARE_TIME_CICLE:
		electronics_instance.set_led("blue", "blink")

		action = VAction()
		server = ServerTrades()
		resp   = server.set_online_aware(action.get_cam_id(), action.get_token())
		if resp:
			if resp['shutdown']:
				#os.system("sudo shutdown -h now")
				pass
			if resp['reboot']:
				os.system("sudo reboot")

		#implement to send the cam online notification
		electronics_instance.set_led("blue", "off")
		aware_online_time = time.time()


def process_image(capture, electronics_instance):
	global sending_image

	file = "rawtemp.png"
	cv2.imwrite(file, capture)

	with open(file, 'rb') as f:
		image = f.read()

	action = VAction()

	server = ServerTrades()
	# implement code to send image to server
	electronics_instance.set_led("blue", "blink")
	resp = server.post_image_to_server(image, action.get_cam_id(), action.get_token())
	electronics_instance.set_led("blue", "off")
	if not resp['token_set']:
	    #storing token locally
	    action.store_token(resp['token'])

	sending_image = False


def on_motion(electronics_instance):
	global sending_image
	# checking the camera and sending the image to the server within a thread
	cam = cv2.VideoCapture(0)
	def get_image():
		retval, im = cam.read()
		return im

	ramp_framse = 10

	for i in xrange(ramp_framse):
		get_image()

	while True:
		capture = get_image()
		if not sending_image:
			sending_image = True
			
			send_image_thread = threading.Thread(target=process_image, args=(capture, electronics_instance))
			send_image_thread.start()

		if not electronics_instance.get_motion():
			break
		sleep(0.1)

"""
Main vigihub application
==>>by R4yG<==
"""

if __name__ == "__main__":

	electronics = Electronics(PROD=True);
	#setting normal starting
	send_cam_online_aware(electronics)
	
	electronics.set_led("white", "on")

	# when motion detected, turn on the light and send a picture each 500 ms
	while True:

		if electronics.get_motion():
			print("Motion detected")
			electronics.set_led("red", "blink")
			on_motion(electronics)
		else:
			print("No Motion detected")
			electronics.set_led("red", "off")
			send_cam_online_aware(electronics)

		sleep(0.1)
